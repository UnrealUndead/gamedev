import qbs

Application
{
    cpp.includePaths: ["include"]
    cpp.cxxLanguageVersion: "c++17"
    //consoleApplication: true
    builtByDefault: true

    files: ["main.cpp", "include/**", "src/**"]

    Depends { name: "cpp" }
    Depends { name: "sfml" }
    Depends { name: "Engine" }
    Depends { name: "Utility" }

    sfml.modules: ["system", "graphics", "window"]
}
