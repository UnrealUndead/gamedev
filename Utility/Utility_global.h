#pragma once

#ifdef _WIN32
#   ifdef USING_UTILITY
#       define EXPORT_UTILITY __declspec(dllexport)
#   else
#       define EXPORT_UTILITY __declspec(dllimport)
#   endif
#elif
#    define EXPORT_UTILITY
#endif
