#include "catch.hpp"

#include "TestUtilities.h"

#include <ECS/Component.h>
#include <ECS/ComponentsContainer.h>
#include <ECS/EntityManager.h>
#include <ECS/Entity.h>

TEST_CASE("addEntity", "[ECS]")
{
    test::EntityManager manager;

    auto entity = manager.addEntity();
    REQUIRE(manager.m_entities.size() == 1);
    CHECK(entity.id == 0);

    auto entity_it = manager.m_entities.find(entity);
    REQUIRE_FALSE(entity_it == manager.m_entities.end());

    manager.addComponent<test::DummyComponent>(entity);

    auto& index_container = entity_it->second;
    REQUIRE(index_container.size() == engine::typeIdOf<test::DummyComponent>() + 1);

    SECTION("Invalid entity")
    {
        engine::Entity invalid_entity(test::INVALID_INDEX);
        CHECK(manager.m_entities.find(invalid_entity) == manager.m_entities.end());
    }

    SECTION("Multiple entities")
    {
        auto second_entity = manager.addEntity();
        REQUIRE(manager.m_entities.size() == 2);
        CHECK(second_entity.id == 1);

        auto second_entity_it = manager.m_entities.find(second_entity);
        REQUIRE_FALSE(second_entity_it == manager.m_entities.end());

        auto& second_index_container = second_entity_it->second;
        REQUIRE(second_index_container.size() == 0);
    }

    SECTION("Multiple managers")
    {
        test::EntityManager second_manager;

        auto first_entity = second_manager.addEntity();
        REQUIRE(manager.m_entities.size() == 1);
        CHECK(first_entity.id == 0);
    }
}

TEST_CASE("addComponent", "[ECS]")
{
    test::EntityManager manager;

    auto entity = manager.addEntity();
    auto second_entity = manager.addEntity();

    SECTION("Invalid entity")
    {
        engine::Entity invalid_entity(test::INVALID_INDEX);
        CHECK_FALSE(manager.addComponent<test::DummyComponent>(invalid_entity));
    }

    SECTION("")
    {
        auto component = manager.addComponent<test::MockComponent>(entity, test::RANDOM_INDEX);
        REQUIRE(component);
        CHECK(component->get().m_data == test::RANDOM_INDEX);

        auto gotten_component = manager.getComponent<test::MockComponent>(entity);
        CHECK(gotten_component->get().m_data == test::RANDOM_INDEX);

        SECTION("Duplicate component")
        {
            auto duplicate_component = manager.addComponent<test::MockComponent>(entity);
            CHECK_FALSE(duplicate_component);
        }

        SECTION("Multiple components")
        {
            auto second_component = manager.addComponent<test::DummyComponent>(entity);
            CHECK(second_component);

            SECTION("Entity index conatiner")
            {
                manager.addComponent<test::LifeTimeComponent>(entity);
                REQUIRE(engine::typeIdOf<test::LifeTimeComponent>() == 2);

                auto first_component_of_second_entity = manager.addComponent<test::LifeTimeComponent>(second_entity);
                auto& index_container = manager.m_entities[second_entity];

                REQUIRE(index_container.size() == 3);
                CHECK(index_container[0] == -1);
                CHECK(index_container[1] == -1);
                CHECK(index_container[2] == 1);
            }
        }

        SECTION("Entity index conatiner")
        {
            auto second_component = manager.addComponent<test::DummyComponent>(entity);

            auto& index_container = manager.m_entities[entity];

            REQUIRE(index_container.size() == 2);
            CHECK(index_container[0] == 0);
            CHECK(index_container[1] == 0);

            SECTION("Other entities")
            {
                auto& second_index_container = manager.m_entities[second_entity];
                CHECK(second_index_container.size() == 0);
            }
        }
    }
}

TEST_CASE("hasComponent", "[ECS]")
{
    test::EntityManager manager;

    auto entity = manager.addEntity();

    SECTION("Invalid entity")
    {
        engine::Entity entity(test::INVALID_INDEX);
        CHECK_FALSE(manager.hasComponent<test::DummyComponent>(entity));
    }

    SECTION("Entity without components")
    {
        CHECK_FALSE(manager.hasComponent<test::DummyComponent>(entity));
    }

    SECTION("Entity with components")
    {
        manager.addComponent<test::DummyComponent>(entity);
        CHECK(manager.hasComponent<test::DummyComponent>(entity));

        SECTION("Invalid component")
        {
            CHECK_FALSE(manager.hasComponent<test::MockComponent>(entity));
        }
    }
}

TEST_CASE("getComponent", "[ECS]")
{
    test::EntityManager manager;

    auto entity = manager.addEntity();

    SECTION("Invalid entity")
    {
        engine::Entity invalid_entity(test::INVALID_INDEX);
        CHECK_FALSE(manager.getComponent<test::MockComponent>(invalid_entity));
    }

    SECTION("No components")
    {
        auto component = manager.getComponent<test::DummyComponent>(entity);
        CHECK_FALSE(component);
    }

    SECTION("")
    {
        manager.addComponent<test::MockComponent>(entity, test::RANDOM_INDEX);

        auto component = manager.getComponent<test::MockComponent>(entity);
        REQUIRE(component);
        CHECK(component->get().m_data == test::RANDOM_INDEX);

        SECTION("Invalid component")
        {
            auto component = manager.getComponent<test::DummyComponent>(entity);
            CHECK_FALSE(component);
        }
    }

}

TEST_CASE("ComponentManager", "[ECS]")
{
    test::EntityManager manager;

    auto entity = manager.addEntity();

    manager.addComponent<test::MockComponent>(entity, test::RANDOM_INDEX);

    SECTION("No components")
    {
        auto& components = manager.m_components_container.getComponents<test::DummyComponent>();
        CHECK(components.empty());
    }

    SECTION("")
    {
        auto& componets = manager.m_components_container.getComponents<test::MockComponent>();
        REQUIRE(componets.size() == 1);
        CHECK(componets[0].m_data == test::RANDOM_INDEX);
    }
}

TEST_CASE("Destructor", "[ECS]")
{
    auto manager = new test::EntityManager();

    SECTION("Components deleted")
    {
        REQUIRE(test::LifeTimeComponent::OBJECTS_ALIVE == 0);

        SECTION("Duplicate component")
        {
            auto entity = manager->addEntity();
            REQUIRE(test::LifeTimeComponent::OBJECTS_ALIVE == 0);

            manager->addComponent<test::LifeTimeComponent>(entity);
            CHECK(test::LifeTimeComponent::OBJECTS_ALIVE == 1);

            manager->addComponent<test::LifeTimeComponent>(entity);
            CHECK(test::LifeTimeComponent::OBJECTS_ALIVE == 1);

            delete manager;
            CHECK(test::LifeTimeComponent::OBJECTS_ALIVE == 0);
        }

        SECTION("Multiple entities")
        {
            for (int i = 1; i <= 3; i++)
            {
                auto entity = manager->addEntity();
                manager->addComponent<test::LifeTimeComponent>(entity);
                CAPTURE(i)
                CHECK(test::LifeTimeComponent::OBJECTS_ALIVE == i);
            }

            delete manager;
            CHECK(test::LifeTimeComponent::OBJECTS_ALIVE == 0);
        }
    }
}

