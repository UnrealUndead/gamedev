#include <algorithm>

namespace util
{
    template<class Container, class Element>
    inline bool contains(const Container& container, const Element& element)
    {
        return std::find(std::begin(container), std::end(container), element) != std::end(container);
    }
}
