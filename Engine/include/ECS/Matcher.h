#pragma once

#include "Engine_global.h"

#include "ECS/Component.h"
#include "ECS/ComponentId.h"
#include "ECS/Entity.h"

#include <iostream>
#include <algorithm>
#include <set>
#include <map>
#include <vector>

namespace engine
{
    class Matcher
    {
    public:
        template<typename ...ComponentTypes>
        void include();
        template<typename ...ComponentTypes>
        void exclude();

        EXPORT_ENGINE
        auto match(const std::map<Entity, std::vector<int>>& entities) const -> std::set<Entity>;

    private:
        template <class ComponentType>
        void includeOne();
        template <class ComponentType>
        void excludeOne();

        bool matchOne(const std::set<int>& componentTypes) const;

        std::set<ComponentId> m_included_types;
        std::set<ComponentId> m_excluded_types;
    };
}

namespace engine
{
    template<typename ...ComponentTypes>
    void Matcher::include()
    {
        // The next line unpacks and executes the funciton on each type
        int unsused[] = { (includeOne<ComponentTypes>(), 0)... };
        (void) unsused; // Surpress compiler warning
    }

    template<typename ...ComponentTypes>
    void Matcher::exclude()
    {
        // The next line unpacks and executes the funciton on each type
        int unsused[] = { (excludeOne<ComponentTypes>(), 0)... };
        (void) unsused; // Surpress compiler warning
    }

    template <class ComponentType>
    void Matcher::includeOne()
    {
        auto type_id = typeIdOf<ComponentType>();
        m_included_types.insert(type_id);
        m_excluded_types.erase(type_id);
    }

    template <class ComponentType>
    void Matcher::excludeOne()
    {
        auto type_id = typeIdOf<ComponentType>();
        m_excluded_types.insert(type_id);
        m_included_types.erase(type_id);
    }
}
