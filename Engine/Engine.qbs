import qbs

DynamicLibrary
{
    name: "Engine"
    targetName: "GameEngine"

    cpp.includePaths: [product.sourceDirectory, "include"]
    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: ["USING_" + product.name.toUpperCase()]
    builtByDefault: true

    files: ["*.h", "include/**", "src/**"]

    Depends { name: "cpp" }
    Depends { name: "sfml" }
    Depends { name: "Utility" }

    sfml.modules: ["system", "graphics", "window"]

    Export
    {
        Depends { name: "cpp" }
        cpp.includePaths: [product.sourceDirectory, "include"]
    }

    Group
    {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }
}
