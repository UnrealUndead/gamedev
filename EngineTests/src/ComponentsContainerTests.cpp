#include "catch.hpp"

#include "TestUtilities.h"

#include <ECS/Component.h>
#include <ECS/ComponentsContainer.h>

TEST_CASE("tryCreateComponentContainer", "[ECS]")
{
    test::ComponentsContainer manager;

    REQUIRE(manager.tryCreateComponentContainer<test::MockComponent>());

    SECTION("Duplicate component")
    {
        CHECK_FALSE(manager.tryCreateComponentContainer<test::MockComponent>());
    }

    SECTION("")
    {
        auto* ptr = manager.m_component_map[engine::typeIdOf<test::MockComponent>()].get();
        REQUIRE(ptr);
        auto* component_container = reinterpret_cast<std::vector<test::MockComponent>*>(ptr);
        CHECK(component_container->empty());
    }
}
