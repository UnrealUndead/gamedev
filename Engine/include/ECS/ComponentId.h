#pragma once

#include "ECS/Component.h"

#include <type_traits>

namespace engine
{
    using ComponentId = std::size_t;

    // Do not use a static variable. See static initialization order fiasco
    inline ComponentId ComponentIdCounter()
    {
        static ComponentId id = 0;
        return id++;
    }

    template<typename ComponentClass>
    inline ComponentId typeIdOf()
    {
        static_assert(std::is_base_of<engine::Component, ComponentClass>::value, "ComponentClass should be of type IComponent.");

        static ComponentId id = ComponentIdCounter();

        return id;
    }
}
