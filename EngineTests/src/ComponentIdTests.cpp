#include "catch.hpp"

#include "TestUtilities.h"

#include <ECS/Component.h>

TEST_CASE("typeIdOf", "[ECS]")
{
    CHECK(engine::typeIdOf<test::MockComponent>() == 0);

    SECTION("Duplicate component")
    {
        CHECK(engine::typeIdOf<test::MockComponent>() == 0);
    }

    SECTION("Multiple components")
    {
         CHECK(engine::typeIdOf<test::DummyComponent>() == 1);
         CHECK(engine::typeIdOf<test::DummyComponent2>() == 2);
         CHECK(engine::typeIdOf<test::DummyComponent3>() == 3);

         CHECK(engine::typeIdOf<test::DummyComponent>() == 1);
         CHECK(engine::typeIdOf<test::DummyComponent3>() == 3);
         CHECK(engine::typeIdOf<test::DummyComponent3>() == 3);
    }
}
