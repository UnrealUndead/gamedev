#pragma once

#include "Engine_global.h"

#include "ECS/Entity.h"
#include "ECS/Component.h"
#include "ECS/ComponentsContainer.h"

#include <vector>
#include <memory>
#include <map>
#include <optional>
#include <cassert>
#include <iostream>

namespace test
{
    struct EntityManager;
}

namespace engine
{
    class  EntityManager
    {
        friend struct test::EntityManager;

    public:
        EXPORT_ENGINE EntityManager();

        inline auto addEntity() -> Entity;
        //void removeEntity(const Entity& entity);

        template<typename ComponentClass, typename... ConstructorArgs>
        auto addComponent(const Entity& entity, ConstructorArgs&&... arguments) -> std::optional<std::reference_wrapper<ComponentClass>>;
        //template<typename ComponentClass>
        //void removeComponent(const Entity& entity);
        template<typename ComponentClass>
        bool hasComponent(const Entity& entity);
        template<typename ComponentClass>
        auto getComponent(const Entity& entity) -> std::optional<std::reference_wrapper<ComponentClass>>;

        ~EntityManager() = default;
        EntityManager(const EntityManager& other) = delete;
        EntityManager(EntityManager&& other) = default;
        EntityManager& operator=(const EntityManager& other) = delete;
        EntityManager& operator=(EntityManager&& other) = default;

    private:
        static constexpr int INVALID_INDEX = -1;

        ComponentsContainer m_components_container; //TODO should probably be a reference

        std::size_t m_entity_id_counter;
        std::map<Entity, std::vector<int>> m_entities;
    };
}

namespace engine
{
    Entity EntityManager::addEntity()
    {
        Entity id { m_entity_id_counter };
        std::vector<int> components_for_entity;
        m_entities.emplace(id, components_for_entity);

        m_entity_id_counter++;

        return id;
    }

    template<typename ComponentClass, typename... ConstructorArgs>
    std::optional<std::reference_wrapper<ComponentClass>> EntityManager::addComponent(const Entity& entity, ConstructorArgs&&... arguments)
    {
        static_assert(std::is_base_of<engine::Component, ComponentClass>::value, "ComponentClass should be of type IComponent.");

        auto entity_it = m_entities.find(entity);
        if (entity_it == m_entities.end())
        {
            return {};
        }

        if (hasComponent<ComponentClass>(entity))
        {
            return {};
        }

        auto& specific_components = m_components_container.getComponents<ComponentClass>();
        specific_components.emplace_back(std::forward<ConstructorArgs>(arguments)...);

        auto component_type_index = typeIdOf<ComponentClass>();

        auto& components_for_entity = entity_it->second;
        if (components_for_entity.size() <= component_type_index)
        {
            components_for_entity.resize(component_type_index + 1, INVALID_INDEX);
        }
        components_for_entity[typeIdOf<ComponentClass>()] = (specific_components.size() - 1);

        return specific_components.back();
    }

    template<typename ComponentClass>
    bool EntityManager::hasComponent(const Entity & entity)
    {
        static_assert(std::is_base_of<engine::Component, ComponentClass>::value, "ComponentClass should be of type IComponent.");

        auto entity_it = m_entities.find(entity);
        if (entity_it == m_entities.end())
        {
            return false;
        }

        auto component_type_index = typeIdOf<ComponentClass>();
        auto& components_for_entity = entity_it->second;
        if (components_for_entity.size() <= component_type_index)
        {
            return false;
        }

        return components_for_entity[component_type_index] != INVALID_INDEX;
    }

    template<typename ComponentClass>
    std::optional<std::reference_wrapper<ComponentClass>> EntityManager::getComponent(const Entity& entity)
    {
        static_assert(std::is_base_of<engine::Component, ComponentClass>::value, "ComponentClass should be of type IComponent.");

        if (!hasComponent<ComponentClass>(entity))
        {
            return {};
        }

        auto& specific_components = m_components_container.getComponents<ComponentClass>();

        const auto& component_index = m_entities[entity][typeIdOf<ComponentClass>()];
        auto& component = specific_components[component_index];
        return std::ref(component);
    }
}
