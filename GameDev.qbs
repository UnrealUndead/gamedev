import qbs

Project
{
    name: "GameDev"

    minimumQbsVersion: "1.7.1"

    references: ["Engine", "EngineTests", "Utility"]
    qbsSearchPaths: "E:/Libraries/qbs/"
}
