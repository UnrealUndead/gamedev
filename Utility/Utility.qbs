import qbs

DynamicLibrary
{
    name: "Utility"
    targetName: "GameEngine"

    cpp.includePaths: [product.sourceDirectory, "include"]
    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: ["USING_" + product.name.toUpperCase()]
    builtByDefault: true

    files: ["*.h", "include/**", "src/**"]

    Depends { name: "cpp" }

    Export
    {
        Depends { name: "cpp" }
        cpp.includePaths: [product.sourceDirectory, "include"]
    }

    Group
    {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }
}
