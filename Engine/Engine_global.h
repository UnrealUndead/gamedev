#pragma once

#ifdef _WIN32
#   ifdef USING_ENGINE
#       define EXPORT_ENGINE __declspec(dllexport)
#   else
#       define EXPORT_ENGINE __declspec(dllimport)
#   endif
#elif
#    define EXPORT_ENGINE
#endif
