#pragma once

#include "Engine_global.h"

namespace engine
{
    class EXPORT_ENGINE Component
    {
    protected:
        Component() = default;
        virtual ~Component() = default;
        Component(const Component& other) = delete;
        Component& operator=(const Component& other) = delete;
        Component(Component&& other) = default;
        Component& operator=(Component&& other) = default;
    };
}
