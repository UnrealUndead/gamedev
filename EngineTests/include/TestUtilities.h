#pragma once

#include <ECS/Component.h>
#include <ECS/ComponentsContainer.h>
#include <ECS/EntityManager.h>

#include <atomic>

namespace test
{
    static constexpr int RANDOM_INDEX = 5;
    static constexpr int INVALID_INDEX = -1;

    struct DummyComponent : public engine::Component
    {
        using engine::Component::Component;
    };

    struct DummyComponent2 : public engine::Component
    {
        using engine::Component::Component;
    };

    struct DummyComponent3 : public engine::Component
    {
        using engine::Component::Component;
    };

    struct MockComponent : public engine::Component
    {
        using engine::Component::Component;

        MockComponent() = default;
        MockComponent(int data)
            : m_data(data)
        {
        }

        int m_data = 0;
    };

    struct LifeTimeComponent : public engine::Component
    {
        static inline std::atomic<int> OBJECTS_ALIVE = 0;

        LifeTimeComponent()
        {
            OBJECTS_ALIVE++;
        }

        ~LifeTimeComponent() override
        {
            OBJECTS_ALIVE--;
        }

        LifeTimeComponent(const LifeTimeComponent& other) = delete;
        LifeTimeComponent& operator=(const LifeTimeComponent& other) = delete;
        LifeTimeComponent(LifeTimeComponent&& other)
        {
            OBJECTS_ALIVE++;
        }
        LifeTimeComponent& operator=(LifeTimeComponent&& other) = delete;
    };

    struct ComponentsContainer : engine::ComponentsContainer
    {
        using engine::ComponentsContainer::ComponentsContainer;
        using engine::ComponentsContainer::tryCreateComponentContainer;
        using engine::ComponentsContainer::m_component_map;
    };

    struct EntityManager : engine::EntityManager
    {
        using engine::EntityManager::EntityManager;
        using engine::EntityManager::m_components_container;
        using engine::EntityManager::m_entities;
    };
}
