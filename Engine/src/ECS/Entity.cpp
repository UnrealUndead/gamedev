#include "ECS/Entity.h"

engine::Entity::Entity(size_t id)
    : id(id)
{

}

engine::Entity::operator size_t() const noexcept
{
    return id;
}

