#include "ECS/Matcher.h"

namespace
{
    const int INVALID_INDEX = -1;

     std::set<int> getValidValuePositions(const std::vector<int>& input)
     {
         std::set<int> output;
         for (std::size_t index = 0; index < input.size(); index++)
         {
             if (input[index] != INVALID_INDEX)
             {
                 output.insert(index);
             }
         }
         return output;
     }
}

std::set<engine::Entity> engine::Matcher::match(const std::map<engine::Entity, std::vector<int> > & entities) const
{
    std::set<Entity> result;
    for (auto [entity, component_indices] : entities)
    {
        if (matchOne(getValidValuePositions(component_indices)))
        {
            result.insert(entity);
        }
    }
    return result;
}

bool engine::Matcher::matchOne(const std::set<int> & componentTypes) const
{
    // Get all elements that are in componentTypes and m_excluded_types
    std::vector<int> disallowed_types;
    std::set_intersection(componentTypes.cbegin(), componentTypes.cend(),
                            m_excluded_types.cbegin(), m_excluded_types.cend(),
                            std::back_inserter(disallowed_types));

    // componentTypes should have all m_included_types
    return disallowed_types.empty() &&
            std::includes(componentTypes.cbegin(), componentTypes.cend(),
                           m_included_types.cbegin(), m_included_types.cend());
}
