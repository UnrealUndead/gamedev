#pragma once

#include "Engine_global.h"

namespace engine
{
    struct EXPORT_ENGINE Entity final
    {
        Entity(size_t id);

        size_t id;

        operator size_t() const noexcept;
    };
}

