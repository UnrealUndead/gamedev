#pragma once

#include "ECS/Component.h"
#include "ECS/ComponentId.h"

#include <vector>
#include <memory>
#include <map>
#include <cassert>
#include <iostream>

namespace test
{
    struct ComponentsContainer;
}

namespace engine
{
    // Note: Used as a RAII wrapper around void*
    using ComponentVector = std::shared_ptr<void>;

    class ComponentsContainer
    {
        friend struct test::ComponentsContainer;

    public:
        ComponentsContainer() = default;

        template<typename ComponentClass>
        auto getComponents() -> std::vector<ComponentClass>&;

        ~ComponentsContainer() = default;
        ComponentsContainer(const ComponentsContainer& other) = delete;
        ComponentsContainer(ComponentsContainer&& other) = default;
        ComponentsContainer& operator=(const ComponentsContainer& other) = delete;
        ComponentsContainer& operator=(ComponentsContainer&& other) = default;

    private:
        template<typename ComponentClass>
        bool tryCreateComponentContainer(); // Returns true if a new container was created.

        std::map<ComponentId, ComponentVector> m_component_map;
    };
}

namespace engine
{
    template<typename ComponentClass>
    std::vector<ComponentClass> &ComponentsContainer::getComponents()
    {
        static_assert(std::is_base_of<engine::Component, ComponentClass>::value, "ComponentClass should be of type IComponent.");

        tryCreateComponentContainer<ComponentClass>();
        return *reinterpret_cast<std::vector<ComponentClass>*>(m_component_map[typeIdOf<ComponentClass>()].get());
    }

    template<typename ComponentClass>
    bool ComponentsContainer::tryCreateComponentContainer()
    {
        static_assert(std::is_base_of<engine::Component, ComponentClass>::value, "ComponentClass should be of type IComponent.");

        auto new_component_container = std::make_shared<std::vector<ComponentClass>>();
        auto insertion_result = m_component_map.try_emplace(typeIdOf<ComponentClass>(), std::move(new_component_container));
        return std::get<bool>(insertion_result);
    }
}
